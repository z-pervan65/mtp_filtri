%Skripta za AD pretvorbu signala
%vrijeme uzorkovanja
t = 0:0.1:20;
%broj bitova
n=10;
%ulazna frekvencija
f = 500;
%kvantizacija
q=f/(2^n-1);
%frekvencija sempliranja
fs = 8000;
%sinusni signal sa amplitudom od 2.5(DC)
y = 2.5+2.5*sin(2*pi*(f/fs)*t);
%Zaokruzivanje na nizi broj
x0 = fix(y/q);
%pretvorba u binarni broj
y0 = dec2bin(x0,n);
%Kvantizirani izlaz
y1 = x0 * q;
%&prikaz rezultata
figure();
plot(t,y);
hold on;
plot(t,y1);
title('A/D pretvorba');
legend('Analogni sinusni signal', 'Kvantizirani sinusni signal');

