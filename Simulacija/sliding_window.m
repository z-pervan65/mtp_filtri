%Student:Pervan Zvonimir
function [ izlVektor ] = sliding_window(ulVektor)
    osjetljivost = 1;
    for i=1:length(ulVektor)
        %Prva vrijednost
        if(i==1)
           %Zbog uzimanja 5 uzoraka, potrebno je dodat dvije vrijednost na
           %po�etak

           %uzorci
           vektor_vr = [ulVektor(i) ulVektor(i+1) ulVektor(i+2) ulVektor(i+3) ulVektor(i+4)];

           %MAD promatranog vektora
           mad_izl = mad(vektor_vr,1);
           %Srednja vrijednost vektora
           sr_vr = mean(vektor_vr);

           %apsolutna vrijednost razlike izlaznih podataka i sr. vr. izlaznih podataka
           apsolutna_vr = abs(vektor_vr - sr_vr);
                      
           %iznad koje vrijednosti da trazi outliere
           zadana_vr = osjetljivost*mad_izl;
           
           %ako je apsolutna vrijednost veca od zadane, tada filtriraj signal
           if(apsolutna_vr(3)>zadana_vr)
                izlaz_novo(i) = sr_vr;
            else
               izlaz_novo(i) = vektor_vr(3);
           end
       %Druga vrijednost
       elseif(i==2)
           prva_v = ulVektor(length(ulVektor)-1);
           vektor_vr = [ulVektor(i-1) ulVektor(i) ulVektor(i+1) ulVektor(i+2) ulVektor(i+3)]; 

           %MAD promatranog vektora
           mad_izl = mad(vektor_vr,1);
           %Srednja vrijednost vektora
           sr_vr = mean(vektor_vr);

           %apsolutna vrijednost razlike izlaznih podataka i sr. vr. izlaznih podataka
           apsolutna_vr = abs(vektor_vr - sr_vr);

           %iznad koje vrijednosti da trazi outliere
           zadana_vr = osjetljivost*mad_izl;
           %ako je apsolutna vrijednost veca od zadane, tada filtriraj signal
           if(apsolutna_vr(3)>zadana_vr)
                izlaz_novo(i) = sr_vr;
           else
               izlaz_novo(i) = vektor_vr(3);
           end
         %Predzadnja vrijednost
        elseif(i==(length(ulVektor)-1))
           vektor_vr = [ulVektor(i-3) ulVektor(i-2) ulVektor(i-1) ulVektor(i) ulVektor(i+1)];
            %MAD promatranog vektora
           mad_izl = mad(vektor_vr,1);
           %Srednja vrijednost vektora
           sr_vr = mean(vektor_vr);

           %apsolutna vrijednost razlike izlaznih podataka i sr. vr. izlaznih podataka
           apsolutna_vr = abs(vektor_vr - sr_vr);

           %iznad koje vrijednosti da trazi outliere
           zadana_vr = osjetljivost*mad_izl;
           %ako je apsolutna vrijednost veca od zadane, tada filtriraj signal
           if(apsolutna_vr(3)>zadana_vr)
                izlaz_novo(i) = sr_vr;
            else
               izlaz_novo(i) = vektor_vr(3);
           end
        %Zadnja vrijednost
        elseif(i==length(ulVektor))
           vektor_vr = [ulVektor(i-4) ulVektor(i-3) ulVektor(i-2) ulVektor(i-1) ulVektor(i)];
            %MAD promatranog vektora
           mad_izl = mad(vektor_vr,1);
           %Srednja vrijednost vektora
           sr_vr = mean(vektor_vr);

           %apsolutna vrijednost razlike izlaznih podataka i sr. vr. izlaznih podataka
           apsolutna_vr = abs(vektor_vr - sr_vr);

           %iznad koje vrijednosti da trazi outliere
           zadana_vr = osjetljivost*mad_izl;
           %ako je apsolutna vrijednost veca od zadane, tada filtriraj signal
           if(apsolutna_vr(3)>zadana_vr)
                izlaz_novo(i) = sr_vr;
           else
               izlaz_novo(i) = vektor_vr(3);
           end
        else
            %uzorci
           vektor_vr = [ulVektor(i-2) ulVektor(i-1) ulVektor(i) ulVektor(i+1) ulVektor(i+2)];

           %MAD promatranog vektora
           mad_izl = mad(vektor_vr,1);
           %Srednja vrijednost vektora
           sr_vr = mean(vektor_vr);

           %apsolutna vrijednost razlike izlaznih podataka i sr. vr. izlaznih podataka
           apsolutna_vr = abs(vektor_vr - sr_vr);
           
           %iznad koje vrijednosti da trazi outliere
           zadana_vr = osjetljivost*mad_izl;
           
           %ako je apsolutna vrijednost veca od zadane, tada filtriraj signal
           if(apsolutna_vr(3)>zadana_vr)
                izlaz_novo(i) = sr_vr;
            else
               izlaz_novo(i) = vektor_vr(3);
           end        
        end
    end
    izlVektor=izlaz_novo;
end

