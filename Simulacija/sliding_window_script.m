close all;
clc;
%Sinusni signal
f = 750;
fs = 8192;
tsin = 0:0.1:100;
%Nezasumljeni sinusni signal
ysin = 2.5e-3+2.5e-3*sin(2*pi*f/fs*tsin);
%zasumljeni sinusni signal
ysin_sum = ysin + 0.50e-3* randn(1, length(ysin));

%medijan filter
izlaz = sliding_window_medijan(ysin_sum);

%prikaz podataka
figure();
plot(tsin,ysin_sum,'g');
hold on;
alpha(.5);
plot(tsin, izlaz,'r');
%title('Medijan filtar');
legend('Ulazni signal','Filtrirani signal');

izlaz1 = izlaz;

for i = 0:100
    izlaz1 = sliding_window_medijan(izlaz1);
end

%prikaz podataka
figure();
alpha(.75);
plot(tsin,ysin_sum,'g');
hold on;
plot(tsin, izlaz1,'r');
%title('Medijan filtar');
legend('Ulazni signal','100 filtriranja');