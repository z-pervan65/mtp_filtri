clear;
close all;
clc;
%Vrijednosti za nisko-propusni filter
Cn=1e-6;
Rn=1000;
%Vrijednosti za visoko-propusni filter
Cv=3.3e-12;
Rv=10000;
%Vrijednosti za pojasno-propusni filter
Cp=1e-6;
Rp=51;
Lp=680e-6;
sim('Simulacija_Pervan_analogni_sinus.slx');

%NISKOPROPUSNI FILTER-----------------------------------------------------
figure();
title('Niskopropusni filter')
subplot(1,3,1);
plot(t,np_1Hz,'b')
legend('1Hz')
hold on;
subplot(1,3,2);
plot(t,np_750Hz,'r')
axis([0  1 0 5])
legend('750Hz')
hold on;
subplot(1,3,3);
plot(t,np_7500Hz,'g')
axis([0  1 0 5])
legend('10000Hz');
figure();
num_np = [1/(Cn*Rn)];
den_np = [1 (1/(Cn*Rn))];
G_np = tf(num_np,den_np);
bode(G_np), grid
title('Bodeov dijagram za NP filtar');
% VISOKOPROPUSNI FILTER ----------------------------------------------------
figure();
subplot(1,3,1);
plot(t,vp_7500Hz,'r')
legend('10000Hz')
axis([0  1 0 5])
hold on;
alpha(0.5)
subplot(1,3,2);
plot(t,vp_750Hz,'b')
legend('750Hz')
axis([0  1 0 5])
hold on;
subplot(1,3,3);
plot(t,vp_1Hz,'g')
axis([0  1 0 5])
alpha(0.5)
legend('1Hz')
title('Visokopropusni filter')
figure();
num_vp = [1 1];
den_vp = [1 -(1/(Cv*Rv))];
G_vp = tf(num_vp,den_vp);
bode(G_vp), grid
title('Bodeov dijagram za VP filtar');
% POJASNO-PROPUSNI FILTER--------------------------------------------------
figure();
subplot(1,3,1);
plot(t,pp_750Hz,'r')
axis([0  1 0 5])
hold on;
subplot(1,3,2);
alpha(0.5)
plot(t,pp_7500Hz,'b')
axis([0  1 0 5])
hold on;
subplot(1,3,3);
plot(t,pp_1Hz,'g')
axis([0  1 0 5])
alpha(0.5)
legend('750Hz','10000Hz','1Hz')
title('Pojasnopropusni filter')
figure();
num_pp = [(Rp/Lp) 1];
den_pp = [1 (Rp/Lp) 1/(Lp*Cp)];
G_pp = tf(num_pp,den_pp);
bode(G_pp), grid
title('Bodeov dijagram za PP filtar');